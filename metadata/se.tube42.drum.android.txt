Categories:Multimedia
License:GPLv2
Web Site:https://github.com/tube42/drumon/blob/HEAD/README.rst
Source Code:https://github.com/tube42/drumon
Issue Tracker:https://github.com/tube42/drumon/issues

Auto Name:Drum On!
Summary:Use you smartphone as a virtual drum
Description:
Simple and minimalist drum app that can, for example, be used to accompany
guitar or bass players.
.

Repo Type:git
Repo:https://github.com/tube42/drumon.git

Build:0.1.3,13
    commit=v0.1.3
    submodules=yes
    build=ant setup

Build:0.1.4,14
    commit=v0.1.4
    submodules=yes
    build=ant setup

Build:0.1.5,15
    commit=v0.1.5
    submodules=yes
    build=ant setup

Build:0.1.6,16
    commit=v0.1.6
    submodules=yes
    build=ant setup

Build:0.1.7,17
    commit=v0.1.7
    submodules=yes
    build=ant setup

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.1.7
Current Version Code:17
