Categories:Development,Security
License:GPLv3+
Web Site:
Source Code:https://github.com/cSploit/android
Issue Tracker:https://github.com/cSploit/android/issues

Auto Name:cSploit
Summary:Network analysis and penetration toolkit
Description:
cSploit is an Android network analysis and penetration suite which aims to offer
to IT security experts/geeks the most complete and advanced professional toolkit
to perform network security assesments on a mobile device.

Once cSploit is started, you will be able to easily map your network,
fingerprint alive hosts operating systems and running services, search for known
vulnerabilities, crack logon procedures of many tcp protocols, perform man in
the middle attacks such as password sniffing (with common protocols dissection),
real time traffic manipulation, etc.

This app requires a rooted device with Busybox installed.
.

Requires Root:yes

Repo Type:git
Repo:https://github.com/cSploit/android

Build:1.5.4,1
    disable=ndk issues, see https://github.com/cSploit/android/issues/37#issuecomment-143089180
    commit=v1.5.4
    subdir=cSploit
    submodules=yes
    gradle=yes
    scandelete=cSploit/jni
    buildjni=yes

Maintainer Notes:
Uses scandelete to get rid of many unused binaries in the submodules.
.

Auto Update Mode:None
Update Check Mode:Tags
Current Version:1.6.0
Current Version Code:2
