Categories:Internet
License:MPL2
Web Site:https://syncthing.net
Source Code:https://github.com/syncthing/syncthing-android
Issue Tracker:https://github.com/syncthing/syncthing-android/issues
Donate:https://tip4commit.com/github/syncthing/syncthing-android

Auto Name:Syncthing
Summary:File synchronization
Description:
Syncthing replaces proprietary sync and cloud services with something open,
trustworthy and decentralized. Your data is your data alone and you deserve to
choose where it is stored, if it is shared with some third party and how it's
transmitted over the Internet.
.

Repo Type:git
Repo:https://github.com/syncthing/syncthing-android.git

Build:0.4.8,22
    commit=80072194b94e0ca0f81e5bc3b88c8f88867e2e5c
    submodules=yes
    gradle=fat

Build:0.4.9,23
    commit=0.4.9
    submodules=yes
    gradle=fat

Build:0.5.0-beta2,24
    commit=0.5.0-beta2
    submodules=yes
    gradle=fat

Build:0.4.11,25
    commit=0.4.11
    submodules=yes
    gradle=fat

Build:0.4.12,26
    commit=0.4.12
    submodules=yes
    gradle=fat

Build:0.4.13,27
    commit=0.4.13
    submodules=yes
    gradle=fat

Build:0.4.14,28
    commit=0.4.14
    submodules=yes
    gradle=fat

Build:0.4.15,29
    commit=0.4.15
    submodules=yes
    gradle=fat

Build:0.4.16,30
    commit=0.4.16
    submodules=yes
    gradle=fat

Build:0.4.17,31
    commit=0.4.17
    submodules=yes
    gradle=fat

Build:0.4.18,32
    commit=0.4.18
    submodules=yes
    gradle=fat

Build:0.5.0-beta4,33
    commit=0.5.0-beta4
    submodules=yes
    gradle=fat

Build:0.5.0-beta5,34
    commit=0.5.0-beta5
    submodules=yes
    gradle=fat

Build:0.5.0-beta6,35
    commit=0.5.0-beta6
    submodules=yes
    gradle=fat

Build:0.5.0-beta7,36
    commit=0.5.0-beta7
    submodules=yes
    gradle=fat

Build:0.5.0,37
    commit=0.5.0
    submodules=yes
    gradle=fat

Build:0.5.1,38
    commit=0.5.1
    submodules=yes
    gradle=fat

Build:0.5.2,39
    commit=0.5.2
    submodules=yes
    gradle=fat

Build:0.5.4,41
    commit=0.5.4
    submodules=yes
    gradle=fat

Build:0.5.5,42
    commit=0.5.5
    submodules=yes
    gradle=fat

Build:0.5.6,43
    commit=0.5.6
    submodules=yes
    gradle=fat

Build:0.5.7,44
    commit=0.5.7
    submodules=yes
    gradle=fat

Build:0.5.8,45
    commit=0.5.8
    submodules=yes
    gradle=fat

Build:0.5.9,46
    commit=0.5.9
    submodules=yes
    gradle=fat

Build:0.5.10,47
    commit=0.5.10
    submodules=yes
    gradle=fat

Build:0.5.11,48
    commit=0.5.11
    submodules=yes
    gradle=fat

Build:0.5.12,49
    commit=0.5.12
    submodules=yes
    gradle=fat

Build:0.5.13,50
    commit=0.5.13
    submodules=yes
    gradle=fat

Build:0.5.14,51
    disable=failing build
    commit=0.5.14
    submodules=yes
    gradle=fat

Build:0.5.15,52
    commit=0.5.15
    submodules=yes
    gradle=fat

Build:0.5.16,53
    commit=0.5.16
    submodules=yes
    gradle=fat

Build:0.5.17,54
    commit=0.5.17
    submodules=yes
    gradle=fat

Build:0.5.18,55
    commit=0.5.18
    submodules=yes
    gradle=fat

Build:0.5.20,57
    commit=0.5.20
    submodules=yes
    gradle=fat

Build:0.5.21,58
    commit=0.5.21
    submodules=yes
    gradle=fat

Build:0.5.22,59
    commit=0.5.22
    submodules=yes
    gradle=fat

Build:0.5.24,61
    commit=0.5.24
    submodules=yes
    gradle=fat

Build:0.5.26,63
    commit=0.5.26
    submodules=yes
    gradle=fat

Build:0.5.27,64
    commit=0.5.27
    submodules=yes
    gradle=fat

Build:0.5.28,65
    commit=0.5.28
    submodules=yes
    gradle=fat

Build:0.6.0,66
    commit=0.6.0
    submodules=yes
    gradle=fat

Build:0.6.1,67
    commit=0.6.1
    submodules=yes
    gradle=fat

Build:0.6.2,68
    commit=0.6.2
    submodules=yes
    gradle=fat

Build:0.6.3,69
    commit=0.6.3
    submodules=yes
    gradle=fat

Build:0.6.4,70
    commit=0.6.4
    submodules=yes
    gradle=fat

Build:0.6.5,71
    commit=0.6.5
    submodules=yes
    gradle=fat

Build:0.6.6,72
    commit=0.6.6
    submodules=yes
    gradle=fat

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:0.6.6
Current Version Code:72
