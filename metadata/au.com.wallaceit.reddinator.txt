Categories:Internet
License:GPLv3+
Web Site:
Source Code:https://github.com/micwallace/reddinator
Issue Tracker:https://github.com/micwallace/reddinator/issues

Auto Name:Reddinator
Summary:Reddit Widget
Description:
A widget for the popular Reddit website.

Features:

* Display any Reddit subreddit in the widget.
* Choose from popular subreddits or search for your favourites.
* Keep you favourite subreddits on your "My Subreddits" list.
* Select the subreddit sorting type (hot, popular, etc).
* Reload the feed.
* Load more feed items.
* Have multiple widgets displaying a different subreddit and sorting type.
* Display thumbnails preference for each widget, choose position of thumbnail.
* Hide post infomation
* Choose from three item click actions. You can open the combined "Reddinator view", open the item's content page or open the item's Reddit page.
* Reddinator view combines the items content and the reddit comments page into a tabbed layout, read the article and participate in Reddits great community discussion at the same time.
.

Repo Type:git
Repo:https://github.com/micwallace/reddinator.git

Build:2.0,9
    commit=23fc94481ee5b571965
    subdir=reddinator
    init=rm -f ../build.gradle
    gradle=yes

Build:2.1,10
    disable=gradle/resource issues
    commit=2.1
    subdir=reddinator
    init=rm -f ../build.gradle
    gradle=yes
    prebuild=sed -i -e 's/support-v4:+/support-v4:19.0.+/g' -e 's/appcompat-v7:+/appcompat-v7:19.0.+/g' build.gradle

Build:2.3,12
    commit=2.3
    subdir=reddinator
    init=rm -f ../build.gradle
    gradle=yes

Build:2.4,13
    commit=2504bf520ec7e6ea0eca3b9b65dbb45ff1961819
    subdir=reddinator
    init=rm -f ../build.gradle
    gradle=yes

Build:2.5,14
    commit=698bf15101b66df22c2d9bd1f7febae6927b5714
    subdir=reddinator
    init=rm -f ../build.gradle
    gradle=yes

Build:2.5,17
    commit=83856fb5520085249583945ff8e4976a5c67c555
    subdir=reddinator
    init=rm -f ../build.gradle && \
        sed -i -e '/bintray/d' build.gradle
    gradle=yes

Build:2.6,19
    commit=99daf481b5ca5888f5f4a89de19f12f4ec509c65
    subdir=reddinator
    init=rm -f ../build.gradle && \
        sed -i -e '/bintray/d' build.gradle
    gradle=yes

Build:2.6,20
    commit=932d61cf311ceacdce0d8d314f0b3cab5dd67117
    subdir=reddinator
    init=rm -f ../build.gradle && \
        sed -i -e '/bintray/d' build.gradle
    gradle=yes

Build:2.6-20150520,21
    commit=85b05342db29fb4f8ea0ec4c4238e3b5994f2312
    subdir=reddinator
    init=rm -f ../build.gradle && \
        sed -i -e '/bintray/d' build.gradle
    gradle=yes
    forceversion=yes

Build:2.6-20150531,22
    commit=2db606702eb0732a4ebd016dd7c6365a7d0642e2
    subdir=reddinator
    init=rm -f ../build.gradle && \
        sed -i -e '/bintray/d' build.gradle
    gradle=yes
    forceversion=yes

Build:2.8,24
    commit=2.8
    subdir=reddinator
    init=sed -i -e '/bintray/d' build.gradle
    gradle=yes
    forceversion=yes
    rm=build.gradle,build

Auto Update Mode:Version %v
Update Check Mode:Tags
Current Version:2.8
Current Version Code:24
